-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (quilt)
Source: udisks2
Binary: udisks2, udisks2-doc, libudisks2-0, libudisks2-dev, gir1.2-udisks-2.0
Architecture: linux-any all
Version: 2.1.8-1
Maintainer: Utopia Maintenance Team <pkg-utopia-maintainers@lists.alioth.debian.org>
Uploaders: Michael Biebl <biebl@debian.org>, Martin Pitt <mpitt@debian.org>,
Homepage: https://www.freedesktop.org/wiki/Software/udisks
Standards-Version: 3.9.8
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-utopia/udisks2.git
Vcs-Git: https://anonscm.debian.org/git/pkg-utopia/udisks2.git
Testsuite: autopkgtest
Testsuite-Triggers: cryptsetup-bin, dosfstools, gir1.2-glib-2.0, kpartx, libatasmart-bin, lvm2, make, mdadm, policykit-1, python3-dbus, python3-gi, reiserfsprogs, xfsprogs
Build-Depends: debhelper (>= 9), dh-autoreconf, gobject-introspection (>= 1.30), gtk-doc-tools, intltool (>= 0.40.0), gnome-common, libacl1-dev, libatasmart-dev (>= 0.17), libgirepository1.0-dev (>= 1.30), libglib2.0-dev (>= 2.31.13), libgudev-1.0-dev (>= 165), libpolkit-agent-1-dev (>= 0.97), libpolkit-gobject-1-dev (>= 0.97), libsystemd-dev (>= 209), pkg-config, udev (>= 147), xsltproc
Package-List:
 gir1.2-udisks-2.0 deb introspection optional arch=linux-any
 libudisks2-0 deb libs optional arch=linux-any
 libudisks2-dev deb libdevel optional arch=linux-any
 udisks2 deb admin optional arch=linux-any
 udisks2-doc deb doc optional arch=all
Checksums-Sha1:
 8e38580b435986570bb7b784facf387757d2de43 931110 udisks2_2.1.8.orig.tar.bz2
 82773c7a59958fa80574c2e7435a2f525d179459 12684 udisks2_2.1.8-1.debian.tar.xz
Checksums-Sha256:
 da416914812a77e5f4d82b81deb8c25799fd3228d27d52f7bf89a501b1857dda 931110 udisks2_2.1.8.orig.tar.bz2
 bbfd9d722d89e883318cf770d9fa217bd7a7f8dec270b236ae869e986aba645a 12684 udisks2_2.1.8-1.debian.tar.xz
Files:
 501d11c243bd8c6c00650474cd2afaab 931110 udisks2_2.1.8.orig.tar.bz2
 ec8777284782eb5d0fbb13bf8d493954 12684 udisks2_2.1.8-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQIcBAEBCAAGBQJYOBB0AAoJENFO8V2v4RNHunYP+wfxKtaSBxwUO43ykth8FyOd
uA/dcxyq/bP6sVInByQ2xhO1rkcpZp8Dfab9QAHq87Vl+EIEiL3cGKD99ZvXzArJ
F2o8wXb2fG2tLIAc1F1UaXb7v06StEgZ9Y4bbY0qtw9r8vxboywb6vVNW0CfZ4cV
HuevyIyG901/WBODxCeXoy/NKUwJFEO2dGzv64nGrrfZDEaxQGLiP7vfhDUIuwfR
0lsRpl5c4TOv4aw5oGadArf1bhqDQ5lgXxZXaxjXayucxHL3/o7esgvK4c9nYbWD
tLNKvmaDsfsJUOnPuReGdaaRpac/4Ca0Txa2rJRURIebu+WOER5h6hkVYgKP32XF
C9jc9XNAzdFAnt/vMB1LBaWTNWt88ZUiwMdAVSnya0e6MQWnDLjyfEwHnb8Moouq
jPcMpGDGWrtAWfEx0cxsTX3Kgzr3dUxWFguobP8/rP8LIhc43FK78kCZ+tdoAXTc
6BNwO/Ht8/Aqz36almilt7+7Fh4j5KGmTWbr5YRpfGiFkFc9U7wOtflF4NoGfxnr
bcBmcBixSaRjaC+KG+F1+nEal76YWvgdaX+9zmT5K1xtx0xIh9mqSO2MSUBraFas
KCwvS6JDxAdDpAz7mzDEZKzdov8p7tppV4HfRXvYKzXoCE8KrZroQ3jBF9XwIOsq
ka5GMr7sEptkjeXqRQic
=4E3N
-----END PGP SIGNATURE-----
